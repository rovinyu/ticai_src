#ifndef _SZZT_GPIO_H_
#define _SZZT_GPIO_H_

#include <linux/init.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/delay.h>
#include <linux/mm.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <asm/io.h>
#include <asm/sizes.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/miscdevice.h>
#include <linux/workqueue.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/irq.h>
#include <linux/of_gpio.h>
#include <asm/gpio.h>
#include <linux/sched.h>
#include <linux/slab.h>

										 
#define IO_MISC_MAGIC			'm'
#define IO_HOST_OTG_CTRL			_IO(IO_MISC_MAGIC, 0)
#define IO_PR_EN_CTRL			_IO(IO_MISC_MAGIC, 1)


										 
#define		OUT_HIGH	1
#define		OUT_LOW		0

										 
struct misc_data { 
	int otg_gpio;//otg enable control gpio
	int pr_en_gpio;//printer power control gpio
	struct mutex misc_lock;
	
};


#endif /* _SZZT_GPIO_H_ */
