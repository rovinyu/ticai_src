#!/bin/bash

cat $1 | while read line || [ -n "$line" ]
do
	src=`echo $line | awk -F ' ' '{print $1}'`
	#echo $src
	dest=`echo $line | awk -F ' ' '{print $2}'`
	#echo $dest

	if [ ! -f $src ];then
		#echo "this file is not exist!"
		continue
	fi

	destfolder=`dirname $dest`
	#echo $destfolder
        toDir=$OUT$destfolder

	if [ ! -d $toDir ];then
		#echo "this path is not exist!"
		#java -jar javamkdir.jar $dest
                mkdir -p $toDir
	fi

	cp $src $OUT$dest
done
