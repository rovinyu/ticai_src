// --~--~--~--~----~--~--~--~----~--~--~--~----~--~--~--~----~--~--~--~--
// Copyright 2013-2015 Qualcomm Technologies, Inc.
// All rights reserved.
// Confidential and Proprietary – Qualcomm Technologies, Inc.
// --~--~--~--~----~--~--~--~----~--~--~--~----~--~--~--~----~--~--~--~--
#pragma once

#include <mare/pdivide_and_conquer.hh>
#include <mare/pfor_each.hh>
#include <mare/pipeline.hh>
#include <mare/preduce.hh>
#include <mare/pscan.hh>
#include <mare/psort.hh>
#include <mare/ptransform.hh>
#include <mare/tuner.hh>
