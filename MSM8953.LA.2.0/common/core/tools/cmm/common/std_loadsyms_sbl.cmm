//============================================================================
//  Name:                                                                     
//    std_loadsyms_xbl.cmm 
//
//  Description:                                                              
//    Script to load Bootloader image symbols
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who           what, where, why
// --------   ---           ---------------------------------------------------------
// 06/18/2015 JBILLING      Add plusvm for tracing
// 06/04/2015 JBILLING      Bug fix for default buildroot
// 04/27/2015 JBILLING      Overhaul for parsing
// 02/06/2015 JBILLING      Added std_apps functionality
// 02/05/2015 JBILLING      Removed /noclear so that xbl symbols clear after each use
// 02/02/2015 JBILLING      Renamed for xbl. updated for 8996 buildpaths etc.
// 07/09/2014 JBilling      updated logic for use in std_debug scripts
// 05/14/2014 AJCheriyan    Created for Badger64 


//###################Arguments passed #####################
LOCAL &ArgumentLine
ENTRY %LINE &ArgumentLine
LOCAL &UTILITY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11
ENTRY &UTILITY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11

//###################Variables #####################
LOCAL &ELFFILE  &LOCAL_BUILDROOT &LocalElfFile &Elf_Directory

LOCAL &Processor_Root_Name &Image
LOCAL &targetprocessorport

&Processor_Root_Name="boot_images"
&Image="sbl1"
&ELFFILE="NULL" 
&LOCAL_BUILDROOT="NULL"

&Elf_Directory="&Processor_Root_Name"
LOCAL &file &filename &what_to_cut &location &filepath &elf_filename &elf_filepath &elf_fullpath &secondPD_elf_filename
LOCAL &RootElfSuffix &SuffixSearchValue_NonReloc &SuffixSearchValue_Reloc &Suffix_NonReloc &Suffix_Reloc

&RootElfSuffix=".elf"
&SuffixSearchValue_NonReloc=".elf"
&SuffixSearchValue_Reloc=".elf"
&Suffix_NonReloc=".elf"
&Suffix_Reloc=".elf"




//#####################Select Subroutine###################
// Name of the utility we are calling
LOCAL &SUBROUTINE
// Any subroutine specific options
// Default exists for each subroutine


// Input Argument 0 is the name of the utility
&SUBROUTINE="&UTILITY"
&SUBROUTINE=STR.UPR("&SUBROUTINE")
IF !(("&SUBROUTINE"=="HELP"))
(
    GOSUB MAIN &UTILITY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11
    ENTRY &PASS &RVAL0 &RVAL1 &RVAL2
    ENDDO &PASS &RVAL0 &RVAL1 &RVAL2
)

    // This should be created by some top level script. The setupenv for each proc would
    // set this up
    AREA.SELECT
    // Call the required utility
    GOSUB &SUBROUTINE &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11
    ENTRY &PASS &RVAL0 &RVAL1 &RVAL2

    GOTO EXIT



MAIN:
    LOCAL &APPSBUILD &RemoteOption &LiveAttach &loadsecondelf &AlternateELF &extraoption
    ENTRY &APPSBUILD &RemoteOption &LiveAttach &loadsecondelf &AlternateELF &extraoption

    LOCAL &buildroot &elffile
    LOCAL &given_buildroot &default_buildroot &default_elf 


    IF ("&APPSBUILD"!="")
    (
        &given_buildroot="&APPSBUILD"
    )
    ELSE
    (
        &given_buildroot="NULL"
    )
    
    IF ("&BOOT_BUILDROOT"!="")
    (
        &default_buildroot="&BOOT_BUILDROOT"
    )
    ELSE
    (
        &default_buildroot="NULL"
    )
    
    IF ("&AlternateElf"=="")
    (
        &AlternateELF="NULL"
    )
    
    IF ("&BOOT_ELF"!="")
    (
        &default_elf="&BOOT_ELF"
    )
    ELSE
    (
        &default_elf="NULL"
    )

              
            
            
    
    GOSUB SELECT_ELF_FILE &given_buildroot &default_buildroot &AlternateELF &default_elf
    ENTRY &buildroot &elffile


    IF ("&RemoteOption"=="remotely")
    (
        &targetprocessorport=&APPS0_PORT
        GOSUB LOAD_SYMBOLS_ON_ALL_APPS_T32 &buildroot &elffile
    )
    ELSE 
    (
        GOSUB LOAD_SYMBOLS_ON_THIS_T32 &buildroot &elffile
    )
    

    GOTO EXIT

    
    
SELECT_ELF_FILE:
        LOCAL &given_buildroot &default_buildroot &alternateelf &default_elf
        ENTRY &given_buildroot &default_buildroot &alternateelf &default_elf

        LOCAL &rvalue_buildroot &rvalue_elffile 
        
            //If buildroot and  elf given, use those
            IF (OS.DIR("&given_buildroot")&&(FILE.EXIST("&alternateelf")))
            (
                &rvalue_elffile="&alternateelf"
                &rvalue_buildroot="&given_buildroot"
                RETURN &rvalue_buildroot &rvalue_elffile
            )
            
            //If no given build directory, use the default one
            IF (!OS.DIR("&given_buildroot"))
            (
                &given_buildroot="&default_buildroot"
            )
            
            //If just alternate elf is given use that for elf and build root
            //However if we can't parse the path for the buildroot, 
            //Then rely on the GUI to give us a good build root
            IF FILE.EXIST("&alternateelf")
            (
                &rvalue_elffile="&alternateelf"
                GOSUB GET_ELF_BUILDROOT &alternateelf
                ENTRY &rvalue_buildroot
                
                IF OS.DIR("&rvalue_buildroot")
                (
                    PRINT "Using &alternateelf as base buildroot and elf file"
                    RETURN &rvalue_buildroot &rvalue_elffile
                )
                //Else drop to GUI
            )
            ELSE IF FILE.EXIST("&given_buildroot/&default_elf")
            (
                IF STRING.SCAN("&extraoption","silent",0)!=-1
                (
                    &rvalue_elffile=("&given_buildroot/&default_elf")
                    &rvalue_buildroot="&given_buildroot"
                    RETURN &rvalue_buildroot &rvalue_elffile
                )
                ELSE
                (
                    //Nothing. Drop to GUI
                )
            
            )
            //We couldn't find a valid elf path by now. Exit if forcesilent specified.
            ELSE IF STRING.SCAN("&extraoption","forcesilent",0)!=-1
            (
                PRINT %ERROR "std_loadsyms_&Image error! Elf parsing error found and forcesilent specified. Exiting"
                GOTO FATALEXIT
            )
            ELSE
            (
                //Nothing. drop to GUI.
            )

            //////////////////////Use GUI//////////////////////
            
            //Use GUI if AlternateElf not specified or if buildroot directory is wrong
            
            
            IF FILE.EXIST("&given_buildroot/&default_elf")
            (
                
                &filepath=OS.FILE.PATH("&given_buildroot/&default_elf")
                
                DIALOG.FILE "&filepath/*&RootElfSuffix"
                ENTRY &rvalue_elffile
                

                GOSUB GET_ELF_BUILDROOT &rvalue_elffile
                ENTRY &rvalue_buildroot
                //If AlternateElf specified, use that path.
                IF FILE.EXIST("&alternateelf")
                (
                    &rvalue_elffile="&alternateelf"
                    PRINT "Root build is &rvalue_buildroot, but Alternate Elf specified to be used: &rvalue_elffile"
                )
            )
            ELSE IF (OS.DIR("&given_buildroot/&Elf_Directory"))
            (
                
                &filepath="&given_buildroot/&Elf_Directory"
        
                DIALOG.FILE "&filepath/*&RootElfSuffix"
                ENTRY &rvalue_elffile
                
                
                GOSUB GET_ELF_BUILDROOT &rvalue_elffile
                ENTRY &rvalue_buildroot
                //If AlternateElf specified, use that path.
                //This case should only hit if alternate elf specified but bad buildrootparsed out from it.
                IF FILE.EXIST("&alternateelf")
                (
                    &rvalue_elffile="&alternateelf"
                    PRINT "Root build is &rvalue_buildroot, but Alternate Elf specified to be used: &rvalue_elffile"
                )
            )
            ELSE
            (
                DIALOG.FILE "*&RootElfSuffix"
                ENTRY &rvalue_elffile
                
                IF ("&rvalue_elffile"!="")
                (
                            GOSUB GET_ELF_BUILDROOT &rvalue_elffile
                            ENTRY &rvalue_buildroot
                )
                ELSE
                (
                            PRINT "No Elf File selected. Exiting"
                            GOTO EXIT
                )
                
            )
            
            
            //Check if an elf was given
            IF ("&rvalue_elffile"=="")
            (
                PRINT "No Elf File selected. Exiting"
                GOTO EXIT
            )
            
            
            RETURN &rvalue_buildroot &rvalue_elffile
          
            
GET_ELF_BUILDROOT:
            LOCAL &local_elffile &local_buildroot
            ENTRY &local_elffile
            
            LOCAL &location &what_to_cut &filepath
            &location=STRING.SCAN("&local_elffile","&Processor_Root_Name",0)
            &what_to_cut=&location-STRING.LENGTH("&local_elffile")-1
            &filepath=STRING.CUT("&local_elffile",&what_to_cut)

            //If not a legitimate  path, return NULL
            IF (OS.DIR("&filepath")&&("&filepath"!=""))
            (
                &local_buildroot=OS.FILE.PATH(&filepath/&Processor_Root_Name)
            )
            ELSE
            (
                &local_buildroot="NULL"
            )

            
            RETURN &local_buildroot

            
LOAD_SYMBOLS_ON_THIS_T32:
    LOCAL &buildroot &elffile
    ENTRY &buildroot &elffile

            PRINT "Loading Elf &elffile with build root &buildroot"
            
            LOCAL &commandlineoptions
            &commandlineoptions=""
            //include "/nocode" unless pushbinary specified.
            IF (STRING.SCAN("&extraoption","pushbinary",0)==-1)
            (
                &commandlineoptions=" /nocode "+"&commandlineoptions"
            )
            //This for tracing. Loads elf file to T32 pod for faster trace analysis, but takes up space on pod memory
            IF (STRING.SCAN("&extraoption","plusvm",0)!=-1)
            (
                &commandlineoptions=" /plusvm"+"&commandlineoptions"
            )

            d.load.elf &elffile &commandlineoptions /strippart "&Processor_Root_Name" /sourcepath "&buildroot/&Processor_Root_Name"

            
            IF !(FILE.EXIST("&buildroot/&Processor_Root_Name/core/products/scripts/std_scripts.men"))
            (
                RETURN //break if build doesn't have scripts populated.
            )
            
            MENU.RESET
                
            // Load the extensions for the system
            do std_utils EXECUTESCRIPT EXIT &buildroot/&Processor_Root_Name/core/products/scripts/std_extensions.cmm                
            // Load the relative path info for the build
            do std_utils EXECUTESCRIPT EXIT &buildroot/&Processor_Root_Name/core/products/scripts/std_toolsconfig.cmm
            // Program the menus for general debugging
            do std_utils EXECUTESCRIPT EXIT  &buildroot/&Processor_Root_Name/core/products/scripts/std_scripts.men

                
            MENU.REPROGRAM std_commands_apps_&CHIPSET.men    
            
    RETURN

    
    
LOAD_SYMBOLS_ON_ALL_APPS_T32:
    LOCAL &buildroot &elffile
    ENTRY &buildroot &elffile
            PRINT "Loading Elf on remote T32 Session. Elf file &elffile with build root &buildroot"

            LOCAL &commandlineoptions
            &commandlineoptions=""
            //include "/nocode" unless pushbinary specified.
            IF (STRING.SCAN("&extraoption","pushbinary",0)==-1)
            (
                &commandlineoptions=" /nocode "+"&commandlineoptions"
            )
            //This for tracing. Loads elf file to T32 pod for faster trace analysis, but takes up space on pod memory
            IF (STRING.SCAN("&extraoption","plusvm",0)!=-1)
            (
                &commandlineoptions=" /plusvm"+"&commandlineoptions"
            )
            

            do std_apps d.load.elf &elffile &commandlineoptions /strippart "&Processor_Root_Name" /sourcepath "&buildroot/&Processor_Root_Name"

            
            IF !(FILE.EXIST("&buildroot/&Processor_Root_Name/core/products/scripts/std_scripts.men"))
            (
                RETURN //break if build doesn't have scripts populated.
            )
            
            INTERCOM.EXECUTE &targetprocessorport MENU.RESET
            
            // Load the extensions for the system
            do std_intercom_do &targetprocessorport std_utils EXECUTESCRIPT EXIT &buildroot/&Processor_Root_Name/core/products/scripts/std_extensions.cmm
            // Load the relative path info for the build
            do std_intercom_do &targetprocessorport std_utils EXECUTESCRIPT EXIT &buildroot/&Processor_Root_Name/core/products/scripts/std_toolsconfig.cmm
            // Program the menus for general debugging
            do std_intercom_do &targetprocessorport std_utils EXECUTESCRIPT EXIT &buildroot/&Processor_Root_Name/core/products/scripts/std_scripts.men
            // Load anything else from the meta build
            //do std_intercom_do &targetprocessorport std_utils EXECUTESCRIPT EXIT &METASCRIPTSDIR/&Image/std_commands_&Image.men
                
            INTERCOM.EXECUTE &targetprocessorport MENU.REPROGRAM std_commands_apps_&CHIPSET.men
            
            RETURN



help:
HELP:
    AREA.RESET
    AREA.CREATE std_loadsyms_xbl 125. 39.
    AREA.SELECT std_loadsyms_xbl
    WINPOS 0. 29. 125. 39.
    AREA.VIEW std_loadsyms_xbl

    //HEADER "std_cti help window"
    

    
    PRINT " ////////////////////////////////////////////////////////////////////"
    PRINT " /////////////// std_loadsyms_xbl Help ////////////////"
    PRINT " ////////////////////////////////////////////////////////////////////"
    PRINT " Usage: "
    PRINT "     do std_loadsyms_xbl <APPSBUILDPATH> <RemoteOption> <LiveAttach> NULL <AlternateELF> <extraoption>"
    PRINT " Where required arguments are (enter 'NULL' if not used): "
    PRINT "     BUILDROOT   == path to boot build, or 'NULL' "
    PRINT "     RemoteOption  == 'remotely' or 'NULL' - Load elf on all apps t32 windows. Mon and Hyp elf should be copied locally"
    PRINT "     LiveAttach    == 'liveattach' or 'NULL' - T32 is attached to apps"
    PRINT "     loadsecondelf == Always NULL (this keeps commands for all std_loadsyms scripts same"
    PRINT "     AlternateELF  == Load elf file that is different from buildpath given"
    PRINT "                      this can be given as the monitor and hypervisor elfs, e.g."
    PRINT "     extraoption == comma,deliminated,options"
    PRINT "                 Which may be:"
    PRINT "                     pushbinary - pushes binary onto target (useful for simulation environments)"
    PRINT "                     silent - attempts to not use GUI. If paths don't resolve, use GUI"
    PRINT "                     forcesilent - doesn't use GUI. If paths don't resolve, FATALEXIT"
    PRINT "                                   forcesilent is for automation where GUI's cannot be used"
    PRINT "                     plusvm    - For ETM Tracing. Loads elf file to t32 pod for faster trace processing"
    PRINT "                                 So that T32 doesn't need to scan memory for trace symbol matching."
    PRINT "                                 However, it takes up space on the pod that could be used for traces"
    PRINT " "
    PRINT " "
    PRINT "E.G:"
    PRINT " do std_loadsyms_xbl \\mypath remotely NULL NULL pushbinary,silent/forcesilent"
        
    RETURN
    
COPY_ERROR:
    PRINT %ERROR "Error copying &ELFFILE to &temp_location"
    ON ERROR
    GOTO EXIT

EXIT:
    ENDDO

FATALEXIT:
    END





